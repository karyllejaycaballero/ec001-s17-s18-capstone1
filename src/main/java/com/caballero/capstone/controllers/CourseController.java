package com.caballero.capstone.controllers;


import com.caballero.capstone.models.Course;
import com.caballero.capstone.models.CourseEnrollment;
import com.caballero.capstone.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping("/courses")
public class CourseController {
    @Autowired
    CourseService courseService;

    //    Create Course
    @PostMapping("/addCourse")
    public ResponseEntity<Object> addCourse(@RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        courseService.addCourse(stringToken, course);
        return new ResponseEntity<>("Course added successfully.", HttpStatus.CREATED);
    }

    //    Get all course
    @GetMapping("/getAllCourses")
    public ResponseEntity<Object> getAllCourses(){
        return new ResponseEntity<>(courseService.getAllCourses(), HttpStatus.OK);
    }

    //    Delete a course
    @DeleteMapping("/deleteCourse/{courseId}")
    public ResponseEntity<Object> deleteCourse(@PathVariable int courseId, @RequestHeader(value = "Authorization") String stringToken){
        return courseService.deleteCourse(courseId, stringToken);
    }

    //    Update a course
    @PutMapping("/updateCourse/{courseId}")
    public ResponseEntity<Object> updateCourse(@PathVariable int courseId, @RequestHeader(value = "Authorization") String stringToken, @RequestBody Course course){
        return courseService.updateCourse(courseId, stringToken, course);
    }

    //    Get user's courses
    @GetMapping("/myCourses")
    public ResponseEntity<Object> getMyCourses(@RequestHeader(value = "Authorization") String stringToken){
        return new ResponseEntity<>(courseService.getMyCourses(stringToken), HttpStatus.OK);
    }

}

