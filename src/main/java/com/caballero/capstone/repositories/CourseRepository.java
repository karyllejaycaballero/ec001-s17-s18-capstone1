package com.caballero.capstone.repositories;

import com.caballero.capstone.models.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course, Object> {
    Course findByCourseName(String courseName);
}
