package com.caballero.capstone.services;

import com.caballero.capstone.config.JwtToken;
import com.caballero.capstone.models.Course;
import com.caballero.capstone.models.CourseEnrollment;
import com.caballero.capstone.models.User;
import com.caballero.capstone.repositories.CourseRepository;
import com.caballero.capstone.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class CourseServiceImpl implements CourseService{
    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtToken jwtToken;

    //    Add course
    public void addCourse(String stringToken, Course course){

        //Retrieve the "User" object using the extracted username from the JWT Token
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        Course newCourse = new Course();
        newCourse.setCourseName(course.getCourseName());
        newCourse.setCourseDescription(course.getCourseDescription());
        newCourse.setPrice(course.getPrice());
        newCourse.setUser(author);
        courseRepository.save(newCourse);
    }

    //    Get All Courses
    public Iterable<Course> getAllCourses() {
        return courseRepository.findAll();
    }

    //    Delete course
    public ResponseEntity deleteCourse(int courseId, String stringToken){
        Course courseForDeleting = courseRepository.findById(courseId).get();
        String CourseAuthorName = courseForDeleting.getUser().getUsername();
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);
        if(authenticatedUserName.equals(CourseAuthorName)){
            courseRepository.deleteById(courseId);
            return new ResponseEntity<>("Course deleted successfully!", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to delete this course.", HttpStatus.UNAUTHORIZED);
        }

    }

    //    Update course
    public ResponseEntity updateCourse(int courseId, String stringToken, Course course){
        Course courseForUpdating = courseRepository.findById(courseId).get();
//        Get the "author" of the specific course.
        String courseAuthorName = courseForUpdating.getUser().getUsername();
//          Get the "username" from the stringToken to compare it with the username of the current course being edited.
        String authenticatedUserName = jwtToken.getUsernameFromToken(stringToken);

        // check if the username of the authenticated user matches the username of the post's author
        if(authenticatedUserName.equals(courseAuthorName)){
            courseForUpdating.setCourseName(course.getCourseName());
            courseForUpdating.setCourseDescription(course.getCourseDescription());
            courseForUpdating.setPrice(course.getPrice());
            courseRepository.save(courseForUpdating);

            return  new ResponseEntity<>("Course information is now updated.", HttpStatus.OK);
        }
        else{
            return new ResponseEntity<>("You are not authorized to edit this course.", HttpStatus.UNAUTHORIZED);
        }
    }

    // Get user's posts
    public Iterable<Course> getMyCourses(String stringToken){
        User author = userRepository.findByUsername(jwtToken.getUsernameFromToken(stringToken));
        return author.getCourses();
    }

}



