package com.caballero.capstone.services;

import com.caballero.capstone.models.Course;
import com.caballero.capstone.models.CourseEnrollment;
import org.springframework.http.ResponseEntity;

import java.util.Set;

public interface CourseService {
    void addCourse(String stringToken, Course course);
    Iterable<Course> getAllCourses();
    ResponseEntity deleteCourse(int courseId, String stringToken);
    ResponseEntity updateCourse(int courseId, String stringToken, Course course);
    Iterable<Course> getMyCourses(String stringToken);

}
