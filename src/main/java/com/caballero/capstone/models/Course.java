package com.caballero.capstone.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;
@Entity
@Table(name="courses")
public class Course{

    @Id
    @GeneratedValue
    private int courseId;

    @Column
    private String courseName;

    @Column
    private String courseDescription;

    @Column
    private double price;

    @ManyToOne
    @JoinColumn(name="user_id")
    @JsonIgnore
    private User user;

    //SSSS

    public Course(){};

    public Course(int courseId, String courseName, String courseDescription, double price, User user) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.price = price;
        this.user = user;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getCourseId() {
        return courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public double getPrice() {
        return price;
    }

    public User getUser() {
        return user;
    }
    //SSSS
}

