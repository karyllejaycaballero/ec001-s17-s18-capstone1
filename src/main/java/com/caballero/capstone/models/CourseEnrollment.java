package com.caballero.capstone.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="course_enrollments")

public class CourseEnrollment {

    @Id
    @GeneratedValue
    private int courseEnrollmentId;

    @ManyToOne
    @JoinColumn(name = "course_id", referencedColumnName = "courseId")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "userId")
    private User user;
    @Column
    private LocalDateTime dateTimeEnrolled;

    public CourseEnrollment() {
    }

    public CourseEnrollment(int courseEnrollmentId, Course course, User user, LocalDateTime dateTimeEnrolled) {
        this.courseEnrollmentId = courseEnrollmentId;
        this.course = course;
        this.user = user;
        this.dateTimeEnrolled = dateTimeEnrolled;
    }
}
